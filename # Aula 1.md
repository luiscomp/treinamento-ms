# Aula 1
Criando Projeto HR-Worker

### `Pre requisitos: `
- [Lombok](https://projectlombok.org/download)
- [Java 11](https://www.oracle.com/br/java/technologies/javase-jdk11-downloads.html)
- [STS - Spring Tool Suite](https://spring.io/tools)

---
## `Modelo Conceitual`
<p align="center">
    <img src="assets/modelo_conceitual.jpg?v=4&s=200" width="700" alt="logo">
</p>

---

## 1.1 `Criar projeto HR-Worker`
Versão do Spring deve ser a `2.3.8.RELEASE`

### *Dependencias:*
```xml
<!-- SPRING -->
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-web</artifactId>
</dependency>
    
<!-- DATABASE -->
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-data-jpa</artifactId>
</dependency>
<dependency>
    <groupId>com.h2database</groupId>
    <artifactId>h2</artifactId>
    <scope>runtime</scope>
</dependency>

<!-- UTILS -->
<dependency>
    <groupId>org.projectlombok</groupId>
    <artifactId>lombok</artifactId>
    <optional>true</optional>
</dependency>
```

OBS: Teste rodar do projeto

---

## 1.2 `Criar Entidades do Projeto`

`Worker.java`
```java
@Entity
@Table(name = "TB_WORKER")
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
public class Worker {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private String name;
	private Double dailyIncome;
}
```

## 1.3 `Criar Repository do Worker`

`WorkerRepository.java`
```java
public interface WorkerRepository extends JpaRepository<Worker, Long> {

}
```

## 1.4 `Configurar o H2 Database`
Adicionar no arquivo `application.properties` o conteúdo:
```properties
spring.application.name=hr-worker
server.port=8001

spring.datasource.url=jdbc:h2:mem:testdb
spring.datasource.username=sa
spring.datasource.password=
spring.datasource.driverClassName=org.h2.Driver

spring.h2.console.enabled=true
spring.h2.console.path=/h2-console
```

## 1.5 `Inserir valores para testes`
Criar o arquivo `data.sql` na pasta resouce com o conteúdo:
```sql
INSERT INTO TB_WORKER (name, daily_income) VALUES ('Eren Yeager', 200.0);
INSERT INTO TB_WORKER (name, daily_income) VALUES ('Mikasa Ackerman', 348.0);
INSERT INTO TB_WORKER (name, daily_income) VALUES ('Levi Ackerman', 580.0);
```

## 1.6 `Criar Objeto ResponseModel`

`ResponseModel.java`
```java
@Data
@NoArgsConstructor
@RequiredArgsConstructor
@JsonInclude(Include.NON_NULL)
public class ResponseModel {
	@NonNull
	private String status;
	private String message;
	private Object data;
}
```

## 1.7 `Criar o Controller/Service Worker`
| Method | Path | Endpoint | Description |
| ------ | ------ | ------ | ------ |
| `GET` | `/` | findAll | Recupera todos os workers |
| `GET` | `/{id}` | findById | Recupera um Worker pelo ID |

| OBS: Após criar, testar os endpoints

## 1.8 `Configurar Swagger2`
Adicionar Dependências
```xml
<!-- SWAGGER -->
<dependency>
    <groupId>io.springfox</groupId>
    <artifactId>springfox-swagger2</artifactId>
    <version>2.9.2</version>
</dependency>
<dependency>
    <groupId>io.springfox</groupId>
    <artifactId>springfox-swagger-ui</artifactId>
    <version>2.9.2</version>
</dependency>
```