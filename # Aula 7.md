# Aula 7
Adicionando Portas Aleatórias para os microserviços e tolerância a falhas com Hystrix

## 7.1 `Porta Aleatória para Hr-Worker`
### 7.1.1 Configurando Propriedades
Adicionar/Substituir as seguintes configurações:
```properties
server.port=${PORT:0}
eureka.instance.instance-id=${spring.application.name}:${spring.application.instance_id:${random.value}}
```

*ATENÇÃO: Não configurar porta aleatória para o hr-payroll agora, somente depois de configurar api-gateway*

## 7.2 `Hystrix` - Circuit Breaker
### 7.2.1 Configurar Dependências
Adicionar Dependências
```xml
<!-- SPRING CLOUD -->
<dependency>
    <groupId>org.springframework.cloud</groupId>
    <artifactId>spring-cloud-starter-netflix-hystrix</artifactId>
</dependency>
```

### 7.2.2 Habilitar Circuit Breaker
Adicionar na classe principal a seguinte anotação:
```java
@EnableCircuitBreaker
```

### 7.2.3 Adicionar Anotation `@HystrixCommand`
Para controlar uma possível mal chamada de serviço por motivos de queda ou erro ou qualquer outro tipo de não disponibilidade do serviço é necessário configurar um método alternativo para garantir uma resposta esperada ou tratada. Isto é feito a partir de um `fallbackMethod`:

endpoint `getPayment()` em `PaymentController.java`
```java
@HystrixCommand(fallbackMethod = "getAlternative")
@GetMapping("/{workerId}/days/{days}")
public ResponseEntity<ResponseModel<Payment>> getPayment(@PathVariable Long workerId, @PathVariable Integer days) {
    return service.getPayment(workerId, days);
}

public ResponseEntity<ResponseModel<Payment>> getPaymentAlternative(Long workerId, Integer days) {
    return service.getPaymentAlternative(workerId, days);
}
```

## 7.3 `Configurando Timeout do Hystrix`
Adicionar as seguintes configurações:

`application.properties`
```properties
hystrix.command.default.execution.isolation.thread.timeoutInMilliseconds=60000
ribbon.ConnectTimeout=10000
ribbon.ReadTimeout=20000
```