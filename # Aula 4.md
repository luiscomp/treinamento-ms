# Aula 4
Criando balanceamento de carga com Ribbon

---

## 4.1 `Configurar Ribbon`
Adicionar Dependências
```xml
<!-- SPRING CLOUD -->
<dependency>
    <groupId>org.springframework.cloud</groupId>
    <artifactId>spring-cloud-starter-netflix-ribbon</artifactId>
</dependency>
```

## 4.2 `Habilitar RibbonCliente`
Adicionar na classe principal
```java
@EnableRibbonClient(name = "hr-worker")
```

## 4.3 `Habilitando múltiplas portas para o serviço Worker`
Remover o parâmetro `url` da anotação `@FeignClient` para configurar multiplas portas para o serviço worker.

`WorkerFeignClient.java`
```java
@FeignClient(name = "hr-worker", path = "/workers")
```

Configurar multiplas portas para o ribbon

`application.properties`
```properties
hr-worker.ribbon.listOfServers=localhost:8001,localhost:8002
```

## 4.4 `Testando Balanceamento`
### 4.4.1 Preparando info para informar o serviço
Adicionar `Enviroments` no projeto Hr-Worker para verificar instância da aplicação que está sendo usada.

`WorkerController.java`
```java
import org.springframework.core.env.Environment;

@Autowired
private Enviroment env;
```

Adicionar a seguinte linha no endpoint `findById()`
```java
log.info(String.join(":", "PORT", env.getProperty("local.server.port")));
```
### 4.4.2 Configurando Run para rodar duas instâncias no STS
Abrir menu de configuração de execução do projeto

`Run -> RunConfigurations...`
<p align="center">
    <img src="assets/run_configuration.jpg?v=4&s=200" width="700" alt="logo">
</p>

Duplicar a configuração do hr-worker clicando com o botão direito.

Em seguida deve-se adicionar nas configurações as portas fixas que serão executados os serviços adicionando na aba *Arguments* o comando:
```properties
-Dserver.port=<porta>
```
Onde os serviços deverão rodar nas portas `8001` e `8002`
<p align="center">
    <img src="assets/run_arguments.jpg?v=4&s=200" width="700" alt="logo">
</p>
