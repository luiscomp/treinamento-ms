# Aula 9
Centralização da documentação via Api-Gateway

## 9.1 `Configurar Swagger2 no Gateway`
Adicionar Dependências
```xml
<!-- SWAGGER -->
<dependency>
    <groupId>io.springfox</groupId>
    <artifactId>springfox-swagger2</artifactId>
    <version>2.9.2</version>
</dependency>
<dependency>
    <groupId>io.springfox</groupId>
    <artifactId>springfox-swagger-ui</artifactId>
    <version>2.9.2</version>
</dependency>
```

## 9.2 `Criar arquivo de configuração do Swagger`
```java
@EnableSwagger2
@Component
@Primary
@EnableAutoConfiguration
public class SwaggerConfig implements SwaggerResourcesProvider {

	@Override
	public List<SwaggerResource> get() {
		List<SwaggerResource> resources = new ArrayList<>();
		resources.add(swaggerResource("Payroll-MS", "/hr-payroll/v2/api-docs", "2.0"));
		resources.add(swaggerResource("Worker-MS", "/hr-worker/v2/api-docs", "2.0"));
		return resources;
	}
	
	 private SwaggerResource swaggerResource(String name, String location, String version) {
	      SwaggerResource swaggerResource = new SwaggerResource();
	      swaggerResource.setName(name);
	      swaggerResource.setLocation(location);
	      swaggerResource.setSwaggerVersion(version);
	      return swaggerResource;
	 }
}
```