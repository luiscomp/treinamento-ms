# Aula 6
Registrando Worker e Payroll como clients do Eureka Server

## 6.1 `Configurando Hr-Worker`
### 6.1.1 Remover RunConfigurations
Remover RunConfigurarion duplicada no projeto Hr-Worker e voltar a trabalhar com uma única instância

### 6.1.2 Configurar Dependências
Adicionar Dependências
```xml
<!-- SPRING CLOUD -->
<dependency>
    <groupId>org.springframework.cloud</groupId>
    <artifactId>spring-cloud-starter-netflix-eureka-client</artifactId>
</dependency>
```

### 6.1.3 Informar local do servidor Eureka
Para o Cliente saiba onde se registrar é necessário informar onde está o Eureka Server adicionando a seguinte propriedade
z
`application.properties`
```properties
eureka.client.service-url.defaultZone=http://localhost:8761/eureka
```

### 6.1.4 Habilitar Eureka Client
Adicionar na classe principal a seguinte anotação:
```java
@EnableEurekaClient
```

## 6.2 `Configurando Hr-Payroll`
### 6.2.1 Configurar Dependências
Adicionar Dependências
```xml
<!-- SPRING CLOUD -->
<dependency>
    <groupId>org.springframework.cloud</groupId>
    <artifactId>spring-cloud-starter-netflix-eureka-client</artifactId>
</dependency>
```

### 6.2.2 Informar local do servidor Eureka
Para o Cliente saiba onde se registrar é necessário informar onde está o Eureka Server adicionando a seguinte propriedade

`application.properties`
```properties
eureka.client.service-url.defaultZone=http://localhost:8761/eureka
```

### 6.2.3 Habilitar Eureka Client
Adicionar na classe principal a seguinte anotação:
```java
@EnableEurekaClient
```

### 6.2.4 Remover Configuração Ribbon
Remover o seguinte código da classe principal:
```java
@EnableRibbonClient(name = "hr-worker")
```
PS: Ao adicionar o Eureka Client automaticamente já vem incluso o RibbonClient

Remover seguinte configuração:

`application.properties`
```properties
hr-worker.ribbon.listOfServers=localhost:8001,localhost:8002
```
PS: Com o Eureka Discovery, não é mais necessário informar onde está o serviço, pois o mesmo se encarrega se encontrar a partir do serviço registrado.

## 6.3 `Testando`
- Rodar Eureka Server
- Rodar Worker
- Rodar Payroll