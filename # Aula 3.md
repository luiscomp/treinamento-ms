# Aula 3
Criando comunicação com Payroll para buscar um Worker

---

## 3.1 - `RestTemplate`
Biblioteca para fazer requisições REST.

### 3.1.1 Disponibilizar Bean RestTemplate
Criar uma classe de configuração para disponibilizar o Bean RestTemplate
```java
@Bean
public RestTemplate restTemplate() {
    return new RestTemplate()
}
```

### 3.1.2 Importar RestTemplate no PaymentService para fazer chamada no Worker
Configurar uma propriedade para o host do hr-worker

`application.properties`
```properties
hr-worker.host=http://localhost:8001
```

Recuperar propriedade na classe `PaymentService.java`
```java
import org.springframework.beans.factory.annotation.Value;

@Value("${hr-worker.host}")
private String workerHost;
```

Refatorar metodo `getPayment()` para fazer requisição via RestTemplate.
```java
Map<String, String> uriVars = new HashMap<>();
urlVars.put("id", workId);

ResponseModel response = restTemplate.getForObject(workerHost+"/workers/{id}", ResponseModel.class, uriVars);

ObjectMapper mapper = new ObjectMapper();
Worker worker = mapper.convertValue(response.getData(), Worker.class);
```

---

## 3.2 - `FeignClient`
Biblioteca Spring Cloud para configuração de Requisições REST de forma simplificada

### 3.2.1 Adicionar Dependência FeignClient ao Hr-Payroll
```xml
<!-- SPRING CLOUD -->
<dependency>
    <groupId>org.springframework.cloud</groupId>
    <artifactId>spring-cloud-starter-openfeign</artifactId>
</dependency>
```

### 3.2.2 Habilitar FeignCliente
Adicionar a anotação `@EnableFeignClient` no arquivo da aplicação.

### 3.2.3 Implementar interface de comunicação

`WorkerFeignClient.java`
```java
@Component
@FeignClient(name = "hr-worker", url="localhost:8001", path="/workers")
public interface WorkerFeignClient {
    @GetMapping("/{id}")
	ResponseEntity<ResponseModel<Worker>> recuperar(@PathVariable Long id);
}
```
Onde:
| atributo | descricao |
| ------ | ------ |
| `name` | Nome do serviço a ser requisitado |
| `url` | Host do serviço a ser requisitado |
| `path` | Recurso do serviço a ser requisitado |

### 3.2.4 Refatorar requisição do PaymentService com FeignClient
Remover código desnecessário:

`application.properties`
```properties
hr-worker.host=http://localhost:8001
```
`PaymentService.java`
```java
import org.springframework.beans.factory.annotation.Value;

@Value("${hr-worker.host}")
private String workerHost;
```

Deve-se importar a interface do WorkerFeignClient com `@Autowired`.
```java
@Autowired
private WorkerFeignClient workerClient;
```

Refatorar ResponseModel adicionando Generics

`ResponseModel.java`
```java
@Data
@NoArgsConstructor
@RequiredArgsConstructor
@JsonInclude(Include.NON_NULL)
public class ResponseModel<T> {
	@NonNull
	private String status;
	private String message;
	private T data;
	private List<T> list;
}
```

Refatorar metodo `getPayment()` para fazer requisição via FeignClient.
```java
ResponseModel<Payment> response = new ResponseModel<>(SUCESSO);
Worker worker;

ResponseModel<Worker> workerResponse = workerClient.recuperar(workerId).getBody();

if(Objects.isNull(workerResponse)) {
    throw new WorkerNotFoundException("Worker not found.");
}

worker = workerResponse.getData();
response.setData(new Payment(worker.getName(), worker.getDailyIncome(), days));
```