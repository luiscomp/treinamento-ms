# Aula 8
Criando projeto Hr-Api-Gateway com Zuul Proxy

## 8.1 `Criar projeto HR-Api-Gateway`
Versão do Spring deve ser a `2.3.8.RELEASE`

### *Dependencias:*
```xml
<!-- SPRING -->
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-web</artifactId>
</dependency>

<!-- SPRING CLOUD-->
<dependency>
    <groupId>org.springframework.cloud</groupId>
    <artifactId>spring-cloud-starter-netflix-eureka-client</artifactId>
</dependency>
<dependency>
    <groupId>org.springframework.cloud</groupId>
    <artifactId>spring-cloud-starter-netflix-zuul</artifactId>
</dependency>
```

OBS: Teste rodar do projeto

---

## 8.2 `Habilitar Zuul Proxy e Eureka Client`
Adicionar na classe principal as seguintes anotações:
```java
@EnableZuulProxy
@EnableEurekaClient
```

## 8.3 `Configurar Propriedades`
`application.properties`
```properties
spring.application.name=hr-api-gateway
server.port=8765

eureka.client.service-url.defaultZone=http://localhost:8761/eureka

zuul.routes.worker.service-id=hr-worker
zuul.routes.worker.path=/hr-worker/**

zuul.routes.payroll.service-id=hr-payroll
zuul.routes.payroll.path=/hr-payroll/**
```

## 8.4 `Atualizar Host do Swagger`
`SwaggerConfig.java`
```java
new Docket(DocumentationType.SWAGGER_2).host("localhost:8765")
```

## 8.5 `Porta Aleatória para Hr-Payroll`
Adicionar/Substituir as seguintes configurações:

`application.properties`
```properties
server.port=${PORT:0}
eureka.instance.instance-id=${spring.application.name}:${spring.application.instance_id:${random.value}}
```

## 8.6 `Configurando Timeout do Zuul`
Adicionar as seguintes configurações:

`application.properties`
```properties
hystrix.command.default.execution.isolation.thread.timeoutInMilliseconds=60000
ribbon.ConnectTimeout=10000
ribbon.ReadTimeout=20000
```