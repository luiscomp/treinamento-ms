# Aula 2
Criando Projeto HR-Payroll

## 2.1 `Criar projeto HR-Payroll`
Versão do Spring deve ser a `2.3.8.RELEASE`

### *Dependencias:*
```xml
<!-- SPRING -->
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-web</artifactId>
</dependency>

<!-- UTILS -->
<dependency>
    <groupId>org.projectlombok</groupId>
    <artifactId>lombok</artifactId>
    <optional>true</optional>
</dependency>
```

OBS: Teste rodar do projeto

---

## 2.2 `Configurar o H2 Database`
Adicionar no arquivo `application.properties` o conteúdo:
```properties
spring.application.name=hr-payroll
server.port=8010
```

## 2.3 `Criar Entidades do Projeto`
- Payment - `apenas representa um pagamento`

## 2.4 `Criar o Controller/Service Payment`
| Method | Path | Endpoint | Description |
| ------ | ------ | ------ | ------ |
| `GET` | `/{workerId}/days/{days}` | getPayment | Recupera o pagamento de um Worker |

| OBS: Após criar, testar os endpoints

<br>

## 2.4 `Configurar Swagger2`
Adicionar Dependências
```xml
<!-- SWAGGER -->
<dependency>
    <groupId>io.springfox</groupId>
    <artifactId>springfox-swagger2</artifactId>
    <version>2.9.2</version>
</dependency>
<dependency>
    <groupId>io.springfox</groupId>
    <artifactId>springfox-swagger-ui</artifactId>
    <version>2.9.2</version>
</dependency>
```