# Aula 5
Criando projeto Hr-Eureka-Server

## 5.1 `Criar projeto HR-Payroll`
Versão do Spring deve ser a `2.3.8.RELEASE`

### *Dependencias:*
```xml
<!-- SPRING CLOUD-->
<dependency>
    <groupId>org.springframework.cloud</groupId>
    <artifactId>spring-cloud-starter-netflix-eureka-server</artifactId>
</dependency>

<!-- LIB NECESSÁRIA PARA EXECUÇÃO CORRETA APÓS JAVA 8 -->
<dependency>
    <groupId>org.glassfish.jaxb</groupId>
    <artifactId>jaxb-runtime</artifactId>
</dependency>
```

OBS: Teste rodar do projeto

---

## 5.2 `Habilitando Eureka Server`
Adicionar na classe principal a seguinte anotação:
```java
@EnableEurekaServer
```

## 5.3 `Configurar Propriedades`
`application.properties`
```properties
spring.application.name=hr-eureka-server
server.port=8761

eureka.client.register-with-eureka=false
eureka.client.fetch-registry=false
```

OBS: Rodar projeto e acessar o endereço `http://localhost:8761`